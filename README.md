# Gitlab + Redis Cluster #

helm 3 is used

```shell
$ gcloud container clusters create mwas-test --preemptible --enable-autoscaling --max-nodes=10 --min-nodes=1 --zone=europe-west3-a --network=mwas-test --subnetwork=mwas-test --machine-type=n1-standard-2 --cluster-version=1.15
$ cd default_gitlab/
$ kubectl create namespace gitlab && kubectl create namespace application && kubectl create namespace observability
$ helmfile apply
$ watch kubectl get ingress -lrelease=gitlab -n gitlab
# add dns entry
# wait ~10mins until the letsencrypt cert is provisioned
# once you're able to access Gitlab in your browser without any cert warning
$ kubectl -n gitlab get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
# username: root
```


# GDK + Redis Cluster #

This won't work without setting up routing to all Redis Cluster nodes from the host's network namespace (which I haven't done)

## instructions ##

1. install and start `minikube` (I used v1.9.2 and virtualbox 6.1 as hypervisor), it comes with a dynamic provisioner for PVs these days
1. add tiller sa: `kubectl apply -f helm.yaml`
1. initialize helm (use latest version of helm binary, I used v2.16.3): `helm init --service-account helm`
1. `helmfile apply`
1. expose redis on a local tcp port: `kubectl -n application port-forward service/redis-redis-cluster 6379`


1. there's no easy way to reconfigure GDK to use a tcp socket for redis (the way it's possible for postgres for example), using socat: `socat -d -d -d UNIX-LISTEN:/home/mwasilewski-gitlab/workspace/com/gitlab-org/gitlab-development-kit/redis/redis.socket,reuseaddr,fork TCP:127.0.0.1:6379` doesn't work

what sort of works is:
```
3 files changed, 7 insertions(+), 3 deletions(-)
support/templates/cable.yml.erb                    | 2 +-
support/templates/gitlab-workhorse.config.toml.erb | 2 +-
support/templates/resque.yml.erb                   | 6 +++++-

modified   support/templates/cable.yml.erb
@@ -1,6 +1,6 @@
 development:
   adapter: redis
-  url: unix:<%= config.redis_socket %>
+  url: tcp://127.0.0.1:6379
   channel_prefix: development_
 test:
   adapter: redis
modified   support/templates/gitlab-workhorse.config.toml.erb
@@ -1,2 +1,2 @@
 [redis]
-URL = "unix:<%= config.redis_socket %>"
+URL = "tcp://127.0.0.1:6379"
modified   support/templates/resque.yml.erb
@@ -1,2 +1,6 @@
-development: unix:<%= config.redis_socket %>
+development:
+  url: redis://127.0.0.1
+  cluster:
+  - 'redis://127.0.0.1:6379'
+
 test: unix:<%= config.redis_socket %>
```

with the above the Gitlab application errors with inability to connect to all redis cluster shards (which makes sense since only the main service is exposed locally)
